module.exports = {
  transpileDependencies: ["vuetify"],
  css: {
    loaderOptions: {
      sass: {
        additionalData: `@import "@/test/scss/variables.scss";`,
        prependData: `@import "@/test/scss/variables.scss";`,
        data: "@import '@/test/scss/variables.scss'",
      },
      // scss: {
      //   additionalData: `@import "~@/test/scss/variables.scss";`,
      //   prependData: `@import "~@/test/scss/variables.scss";`,
      // },
    },
  },
  chainWebpack: (config) => {
    config.plugin("VuetifyLoaderPlugin").tap((args) => [
      {
        match(originalTag, { kebabTag, camelTag, path, component }) {
          if (kebabTag.startsWith("core-")) {
            return [
              camelTag,
              `import ${camelTag} from '@/components/core/${camelTag.substring(
                4
              )}.vue'`,
            ];
          }
        },
      },
    ]);
  },
};
